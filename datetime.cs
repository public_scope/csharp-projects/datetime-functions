    public static partial class DateTimeExtensions
    {
        public static DateTime FirstDayOfWeek(this DateTime dt)
        {
            var culture = System.Threading.Thread.CurrentThread.CurrentCulture;
            var diff = dt.DayOfWeek - culture.DateTimeFormat.FirstDayOfWeek;
            if (diff < 0)
                diff += 7;
            return dt.AddDays(-diff).Date;
        }

        public static DateTime LastDayOfWeek(this DateTime dt)
        {
            return dt.FirstDayOfWeek().AddDays(6);
        }

        public static DateTime FirstDayOfMonth(this DateTime dt)
        {
            return new DateTime(dt.Year, dt.Month, 1);
        }

        public static DateTime LastDayOfMonth(this DateTime dt)
        {
            return dt.FirstDayOfMonth().AddMonths(1).AddDays(-1);
        }

        public static DateTime FirstDayOfNextMonth(this DateTime dt)
        {
            return dt.FirstDayOfMonth().AddMonths(1);
        }

        public static DateTime FirstDayOfQuarter(this DateTime dt)
        {
            int quarterNumber = (dt.Month - 1) / 3 + 1;
            return new DateTime(dt.Year, (quarterNumber - 1) * 3 + 1, 1);
        }

        public static DateTime LastDayOfQuarter(this DateTime dt)
        {
            int quarterNumber = (dt.Month - 1) / 3 + 1;
            return dt.FirstDayOfQuarter().AddMonths(3).AddDays(-1);
        }
    }

// How to use it, you can now place this in your namespace:
string MessageString = "";
var today = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
var yesterday = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).AddDays(-1);
var firstdayOfThisWeek = today.FirstDayOfWeek();                                // 9/9/2018 12:00:00 AM
var firstdayOfLastWeek = today.AddDays(-7).FirstDayOfWeek();                    // 9/2/2018 12:00:00 AM
var lastDayOfThisWeek = today.LastDayOfWeek();                                  // 9/15/2018 12:00:00 AM
var lastDayOfLastWeek = today.AddDays(-7).LastDayOfWeek();                      // 9/8/2018 12:00:00 AM
var firstdayOfThisMonth = today.FirstDayOfMonth();                              // 9/1/2018 12:00:00 AM
var firstdayOfLastMonth = today.AddMonths(-1).FirstDayOfMonth();                // 8/1/2018 12:00:00 AM
var lastdayOfThisMonth = today.LastDayOfMonth();                                // 9/30/2018 12:00:00 AM
var lastdayOfLastMonth = today.AddMonths(-1).LastDayOfMonth();                  // 8/31/2018 12:00:00 AM
var firstdayOfThisQuarter = today.FirstDayOfQuarter();                          // 7/1/2018 12:00:00 AM
var firstdayOfLastQuarter = today.AddMonths(-3).FirstDayOfQuarter();            // 4/1/2018 12:00:00 AM
var lastdayOfThisQuarter = today.LastDayOfQuarter();                            // 9/30/2018 12:00:00 AM
var lastdayOfLastQuarter = today.AddMonths(-3).LastDayOfQuarter();              // 6/30/2018 12:00:00 AM
var beginingOfThisYear = new DateTime(DateTime.Now.Year, 1, 1);                 // 1/1/2018 12:00:00 AM
var beginingOfLastYear = new DateTime(DateTime.Now.Year, 1, 1).AddYears(-1);    // 1/1/2017 12:00:00 AM
var endOfThisYear = new DateTime(DateTime.Now.Year, 12, 31);                    // 12/31/2018 12:00:00 AM
var endOfLastYear = new DateTime(DateTime.Now.Year, 12, 31).AddYears(-1);       // 12/31/2017 12:00:00 AM

MessageString = "Today is :" + today + "\r\n";
MessageString = MessageString + "Yesterday :" + yesterday + "\r\n";
MessageString = MessageString + "First Day of this Week is :" + firstdayOfThisWeek + "\r\n";
MessageString = MessageString + "First Day of last Week is :" + firstdayOfLastWeek + "\r\n";
MessageString = MessageString + "Last Day of this Week is : " + lastDayOfThisWeek + "\r\n";
MessageString = MessageString + "Last Day of last Week is : " + lastDayOfLastWeek + "\r\n";
MessageString = MessageString + "First Day of this Month is : " + firstdayOfThisMonth + "\r\n";
MessageString = MessageString + "First Day of last Month is : " + firstdayOfLastMonth + "\r\n";
MessageString = MessageString + "Last Day of this Month : " + lastdayOfThisMonth + "\r\n";
MessageString = MessageString + "Last Day of last Month : " + lastdayOfLastMonth + "\r\n";
MessageString = MessageString + "First Day of this Quarter : " + firstdayOfThisQuarter + "\r\n";
MessageString = MessageString + "First Day of last Quarter : " + firstdayOfLastQuarter + "\r\n";
MessageString = MessageString + "Last Day of last Quarter : " + lastdayOfThisQuarter + "\r\n";
MessageString = MessageString + "Last Day of last Quarter : " + lastdayOfLastQuarter + "\r\n";
MessageString = MessageString + "First Day of last Year : " + beginingOfThisYear + "\r\n";
MessageString = MessageString + "First Day of last Year : " + beginingOfLastYear + "\r\n";
MessageString = MessageString + "Last Day of last Year : " + endOfThisYear + "\r\n";
MessageString = MessageString + "Last Day of last Year : " + endOfLastYear + "\r\n";
MessageBox.Show(MessageString);